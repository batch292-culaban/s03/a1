-- CRUD Operations
-- Create
-- Retrieve/Read
-- Update
-- Delete

-- Insert Records (Create)
-- to insert artists record into the artist table:
-- INSERT INTO <table_name>(field_name) VALUES (value)

INSERT INTO artists(name) VALUES ("Post Malone");
INSERT INTO artists(name) VALUES ("Aegis");
INSERT INTO artists(name) VALUES ("Salbakutah");
INSERT INTO artists(name) VALUES ("Journey");
INSERT INTO artists(name) VALUES ("Taylor Swift");
INSERT INTO artists(name) VALUES ("BTS");
INSERT INTO artists(name) VALUES ("Eraserheads");

-- to insert album records into the albums table:
INSERT INTO albums(album_title, date_released, artist_id) VALUES ("E5C4PE","
1981-06-31",4);
INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Fearless","
2008-11-11",5);
INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Wings","
2016-10-10",6);
INSERT INTO albums(album_title, date_released, artist_id) VALUES ("CUTTERPILLOW","
1995-12-08",7);
INSERT INTO albums(album_title, date_released, artist_id) VALUES ("HOLLYWOOD'S BLEEDING","
2019",1);
INSERT INTO albums(album_title, date_released, artist_id) VALUES ("HALIK","
1998",2);
INSERT INTO albums(album_title, date_released, artist_id) VALUES ("S2PID LUV","
2002",3);

-- TO INSERT SONGS RECORDS IN THE SONGS TABLE
INSERT INTO songs (song_name, length, genre, album_id) VALUES ('
Open Arms','00:03:18', 'rock', 1);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ('
You Belong With Me','00:03:52', 'country', 2);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ('
Blood Sweat & Tears','00:03:37', 'moombahton', 3);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ('
Ang Huling El Bimbo ','00:07:30', 'rock', 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ('
Open Arms','00:03:18', 'rock', 1);
